#!/usr/bin/python3
# -*- encoding: utf-8 -*-
# isulatmo.py - A simple virtual notebook for taking notes.
#
# MIT License
#
# Copyright (c) 2018-2019 Jesus Vedasto Olazo
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import shelve
import os
import sys
import re
import time
import shutil

import tkinter as tk
from tkinter import ttk
from tkinter import colorchooser, messagebox, filedialog, simpledialog
from tkinter.scrolledtext import ScrolledText

from PIL import Image, ImageTk
from tkinterhtml import TkinterHtml
import markdown
from html2text import HTML2Text

__version__ = "1.0.0"
__author__ = "Jesus Vedasto Olazo"
__web__ = "http://isulatmo.co.nf/"
__email__ = "jestoy.olazo@gmail.com"

# This is the path for notebook file storage.
NOTE_PATH = os.path.join(os.path.expanduser('~'), "Documents", "IsulatMo", "notes")
# This is a date & time format for any notebook content.
# DATE_FORMAT = "%d/%m/%Y %H:%M:%S"
DATE_FORMAT = "%d/%m/%Y"
# This is the application icon.
ICON = os.path.join(os.getcwd(), 'isulatmo.ico')
# Path for license file.
License_Path = os.path.join(os.getcwd(), 'LICENSE.txt')

IMG_PATH = os.path.join(os.path.expanduser('~'), 'Documents', 'IsulatMo', 'images')

with open('README.md', 'r') as rdmefile:
    data = rdmefile.read()

# This is the default content of the tkinterhtml widget.
# This will fix the extension error loading with linux operating system.
if os.name == 'posix':
    README = markdown.markdown(data, extensions=['markdown.extensions.tables'])
else:
    README = markdown.markdown(data, extensions=['tables'])

images = {}

def imageHandler(img):
    """
    imageHandler function accepts single argument pertaining to the filename of the
    image that currently included in an html document. This function returns the
    image using PIL module. You have to use a desired width and height of the image
    so that it will be rendered as desired not too big and not to small. Otherwise
    use html img tag instead to resize the image. Don't forget to place your image
    im images folder on your home directory.
    """
    
    image = Image.open(os.path.join(IMG_PATH, img))
    photo = ImageTk.PhotoImage(image)
    images[img] = photo
    return photo

def fixItSub(content, mode="html"):
    if mode == "html":
        content = re.sub("<sup>", "--sup--", content)
        content = re.sub("</sup>", "--/sup--", content)
        content = re.sub("<sub>", "--sub--", content)
        content = re.sub("</sub>", "--/sub--", content)
    elif mode=="text":
        content = re.sub("--sup--", "<sup>", content)
        content = re.sub("--/sup--", "</sup>", content)
        content = re.sub("--sub--", "<sub>", content)
        content = re.sub("--/sub--", "</sub>", content)
    return content

def stylePath():
    """
    This function accepts no argument and return the content of a stylesheet
    in string form for later use.
    """
    config_path = os.path.join(os.getcwd(), 'config.txt')

    if os.path.isfile(config_path): # check if default styling has been set.
        with open(config_path, 'r') as configfile:
            data = configfile.read() # if so load it.
        if not data:
            data = 'red.css'
    else: # if not create config.txt and set default styling to red color.
        with open(config_path, 'w') as configfile:
            configfile.write('red.css')
        data = 'red.css'

    css_path = os.path.join(os.getcwd(), 'css', data)
    with open(css_path, 'r') as cssfile: # open the stylesheet from config.txt file.
        css_content = cssfile.read()
    return css_content

# Sets the default styling for the document.
STYLESHEET = stylePath()

class HtmlViewer(TkinterHtml):

    def __init__(self, master=None, **kws):
        TkinterHtml.__init__(self, master, **kws)

    def handlerImage(self, func):
        """
        This function will sets the the value of -imagecmd option and everytime
        an image has been parsed from the document the argument 'func' is a function
        that will extract the image from it and place it the HtmlViewer.
        """
        self.reset()
        self.config(imagecmd=func)

    def style(self, *args):
        """
        This function is use for applying styles using desired stylesheet. The
        default folder is in css and file name style.css. You are free to modified
        the style.css file to your likings but make sure it is CSS version 2 of
        the file.
        """
        return self.tk.call(self._w, "style", *args)

    def set_content(self, html_source):
        """
        This function accept a full text version of the html file and then
        this will be parse and loaded into the widget.
        """
        self.reset()
        self.parse(html_source)

class Notebook(tk.Frame):

    def __init__(self, master=None, **kw):
        tk.Frame.__init__(self, master)
        self.master.geometry("+75+10")
        title = " - ".join(["IsulatMo", __version__])
        self.master.title(title)
        self.master.protocol("WM_DELETE_WINDOW", self.close)
        self.setupUi()
        self.current_note = None
        if os.name == 'nt':
            self.master.state('zoomed')
            self.master.iconbitmap(ICON)
        elif os.name == 'posix':
            self.master.attributes('-zoomed', True)
            self.master.tk.call('wm', 'iconphoto', self.master._w, tk.PhotoImage(file='isulatmo.gif'))

    def setupUi(self):
        menubar = tk.Menu(self)
        self.master.config(menu=menubar)

        filemenu = tk.Menu(menubar, tearoff=0)
        themesmenu = tk.Menu(menubar, tearoff=0)
        helpmenu = tk.Menu(menubar, tearoff=0)
        menubar.add_cascade(label="File", menu=filemenu)
        menubar.add_cascade(label="Themes", menu=themesmenu)
        menubar.add_cascade(label="Help", menu=helpmenu)

        filemenu.add_command(label="New Notebook", command=self.addNewNoteBook)
        filemenu.add_command(label="Delete Notebook",
                             command=self.deleteNoteBook)
        filemenu.add_separator()
        filemenu.add_command(label="New Notes", command=self.addNewNotes)
        filemenu.add_command(label="Edit Notes", command=self.editNotes)
        filemenu.add_command(label="Delete Notes", command=self.deleteNotes)
        filemenu.add_separator()
        filemenu.add_command(label="Quit", command=self.close)

        self.styles = [] # create an empty list for stylesheets.

        for style in os.listdir(os.path.join(os.getcwd(), 'css')):
            if os.path.isfile(os.path.join(os.getcwd(), 'css', style)):
                file_name, ext = os.path.splitext(style)
                if ext == ".css":
                    self.styles.append(file_name.title())

        with open('config.txt', 'r') as configfile:
            data = configfile.read()

        data = data.replace(".css", "")

        self.rad_btn_var = tk.IntVar()

        for idx, style in enumerate(self.styles):
            if style.lower() == data:
                self.rad_btn_var.set(idx)
            themesmenu.add_radiobutton(label=style,value=idx,
                                       variable=self.rad_btn_var,
                                       command=self.changeTheme)

        helpmenu.add_command(label="Help", command=self.showHelp)
        helpmenu.add_command(label="About", command=self.showAbout)
        
        self.paned_win = ttk.PanedWindow(self, orient=tk.HORIZONTAL)
        self.paned_win.pack(expand=True, fill=tk.BOTH)

        self.note_tree = ttk.Treeview(self.paned_win,
                                      columns=('date'))
        self.note_tree.tag_configure('treefont', foreground='gray',
                                     font=('Times', 12)) # foreground='#1F5889'
        self.note_tree.column('date', width=150)
        self.note_tree.heading('date', text='Date')
        self.note_tree.column('#0', width=175)
        self.note_tree.heading('#0', text="Notebook")
        self.note_tree.bind("<<TreeviewSelect>>", self.selectNotes)
        self.note_tree.bind("<Double-Button-1>", self.notesEdit)
        self.paned_win.add(self.note_tree)

        container = tk.Frame(self.paned_win)
        container.pack(expand=True, fill=tk.BOTH)
        
        self.sc_text = HtmlViewer(container)
        self.sc_text.pack(expand=True, fill=tk.BOTH, side=tk.LEFT)
        self.sc_text.handlerImage(imageHandler) # For rendering image in document.
        self.sc_text.set_content(README) # Set the initial content to README.md
        self.sc_text.style(STYLESHEET) # Set the stylesheet of the document.

        vsb = tk.Scrollbar(container, orient=tk.VERTICAL,
                           command=self.sc_text.yview)
        vsb.pack(fill='y', side=tk.RIGHT)

        self.sc_text['yscrollcommand'] = vsb.set
        
        self.paned_win.add(container)

        search_frame = tk.Frame(self)
        search_frame.pack(fill="x")
        self.search_entry = ttk.Entry(search_frame)
        self.search_entry.grid(row=0, column=0)
        self.search_btn = ttk.Button(search_frame, text="Search")
        self.search_btn.grid(row=0, column=1)
        self.search_btn.config(command=self.searchNotes)

        # A hack fix for scroll issue with windows operating system.
        if os.name == 'nt':
            self.sc_text.bind_all('<MouseWheel>', self.scrollHack)

        # This function will insert all the notebook in the treeview.
        self.showNoteBook()

    def changeTheme(self):
        """This accepts no parameters and then change the default theme."""
        global STYLESHEET
        # Get the value of the radiobutton and add an extension of '.css'.
        # Then saves it to file as the default stylesheet.
        style = "".join([self.styles[self.rad_btn_var.get()].lower(), ".css"])
        with open('config.txt', 'w') as configfile:
            configfile.write(style)

        # Open the default location of the stylesheet document and apply.
        with open(os.path.join(os.getcwd(), 'css', style)) as cssfile:
            css_content = cssfile.read()
        STYLESHEET = css_content
        self.sc_text.style(STYLESHEET)

    def scrollHack(self, event):
        """This method fix the scroll issue with windows operating system."""
        if isinstance(event.widget, str):
            widget = self.sc_text.nametowidget(event.widget.replace(".document", ""))
            if event.delta == -120: # Check if scroll down, then scroll the document.
                widget.yview_scroll(6, tk.UNITS)
            elif event.delta == 120: # Check if scroll up then scroll the document.
                widget.yview_scroll(-6, tk.UNITS)

    def editNotes(self):
        """This method opens the notes in edit mode."""
        notes = self.note_tree.focus()
        notebook = self.note_tree.parent(notes)
        if notebook != '':
            if self.checkNoteBook(notebook) == True:
                note_tp = tk.Toplevel(self)
                win = EditNotes(note_tp, notebook, notes)
                win.pack(expand=True, fill=tk.BOTH)
                self.wait_window(note_tp)
                self.showNoteBook()

    def notesEdit(self, event):
        """
        This method is an event binding when user double click on the current note and opens it
        up in edit mode.
        """
        notes = self.note_tree.focus()
        notebook = self.note_tree.parent(notes)
        if notebook != '':
            if self.checkNoteBook(notebook) == True:
                note_tp = tk.Toplevel(self)
                win = EditNotes(note_tp, notebook, notes)
                win.pack(expand=True, fill=tk.BOTH)
                self.wait_window(note_tp)
                self.showNoteBook()

    def searchNotes(self):
        """
        This function accepts no argument, instead it will scroll/move the document up or down
        to look for the word that has been querying.
        """
        notes = self.note_tree.focus()
        notebook = self.note_tree.parent(notes)
        if notebook != '':
            item = self.search_entry.get()
            self.sc_text.update_idletasks()
            try:
                if self.current_note:
                    for line in self.current_note.split('\n'):
                        if item in line:
                            match = re.match("<.*?>", line)
                            if match:
                                counter = 0
                                idx = 0
                                tag = match.group(0)[1:-1]
                                for line in self.current_note.split('\n'):
                                    if (match.group(0) in line) & (item in line):
                                        idx = counter
                                        #print("idx %d" % idx)
                                    elif match.group(0) in line:
                                        counter += 1
                                #print(counter)
                                #print(self.sc_text.search(tag))
                                self.sc_text.yview_name(self.sc_text.search(tag)[idx])
            except:
                print(sys.exc_info())
                #print("Sorry something went wrong.")

    def showAbout(self):
        """
        This function load a toplevel tkinter widget and show information
        pertaining to the software like author, email, version, decription
        etc... This accepts no argument.
        """
        configs = {'name': 'IsulatMo',
                  'version': __version__,
                  'description': 'A simple virtual notebook.',
                  'email': __email__,
                  'author': __author__,
                  'web': __web__,
                  'license': License_Path,
                   'logo': ICON
                  }
        about_tp = tk.Toplevel(self)
        win = About(about_tp, **configs)
        win.pack(expand=True, fill=tk.BOTH)

    def showHelp(self):
        """
        This function accepts no argument and create a toplevel window and load the
        readme file into a tkinterhtml widget.
        """
        help_tp = tk.Toplevel(self)
        config = {'help_file': 'README.md'}
        win = HelpWindow(help_tp, **config)
        win.pack(expand=True, fill=tk.BOTH)

    def selectNotes(self, event):
        """
        This function is an event function that will be called when you select
        something from the treeview widget and shows information if it has in
        HTML format.
        """
        self.sc_text.set_content("<html><head></head><body></body></html>") # Show blank html file as default document.
        self.sc_text.style(STYLESHEET) # Sets the style of the document.
        list_of_files = os.listdir(NOTE_PATH) # create a list of files and folders.
        notebook = [] # Sets an empty list for future requirement.
        for mfile in list_of_files:
            filename, extension = os.path.splitext(mfile)
            if filename not in notebook:
                # This will check if the filename is already in the notebook list if not
                # append the notebook list.
                notebook.append(filename)
        if event.widget.focus() not in notebook:
            # This will check whether the selected item is a note.
            db = shelve.open(os.path.join(NOTE_PATH, event.widget.parent(event.widget.focus())))
            content = db[event.widget.focus()]['content']
            db.close()
            content = fixItSub(content, mode="text")
            self.current_note = content # this will store the current html doc.
            self.sc_text.set_content(content)
            self.sc_text.style(STYLESHEET)
        else:
            # if the selected item is a notebook, create an html document with notebook as a title
            # and list the notes available in the currently selected notebook.
            myheader = "#" + event.widget.focus().title()
            mylist = event.widget.get_children(event.widget.focus())
            for myitem in mylist:
                myheader += "\n\n* [" + myitem + "](" + myitem + ")"
            content = markdown.markdown(myheader)
            self.sc_text.set_content(content)
            self.sc_text.style(STYLESHEET)

    def addNewNotes(self):
        """
        This function handle the addition of new notes and avoid running the
        script if the one you selected is the notes and not the notebook part.
        This will show a toplevel tkinter widget NewNotes.
        """
        notebook = self.note_tree.focus()
        if notebook != '':
            if self.checkNoteBook(notebook) == True:
                note_tp = tk.Toplevel(self)
                win = NewNotes(note_tp, notebook=notebook)
                win.pack(expand=True, fill=tk.BOTH)
                self.wait_window(note_tp)
                self.showNoteBook()

    def checkNoteBook(self, notebook):
        """
        This will accept a string type argument and return True if the selected
        is a notebook and return False otherwise.
        """
        list_of_files = os.listdir(NOTE_PATH)
        mylist = []
        for mfile in list_of_files:
            filename, extension = os.path.splitext(mfile)
            if filename not in mylist:
                mylist.append(filename)
        if notebook not in mylist:
            return False
        else:
            return True

    def addNewNoteBook(self):
        """
        This will show a toplevel tkinter widget with entry widget to create
        new notebook.
        """
        addnew = tk.Toplevel(self)
        win = NewNotebook(addnew)
        win.pack(expand=True, fill=tk.BOTH)
        self.wait_window(addnew)
        self.showNoteBook()
        
    def deleteNoteBook(self):
        """
        This is funtion will delete the notebook from the software. This will
        initially show a pop up to double check whether the said notebook is
        sure to erase if return True delete it else False not to delete it
        and just close the pop up.
        """
        nbook = self.note_tree.focus()
        if self.checkNoteBook(nbook):
            answer = tk.messagebox.askyesno('Delete Notebook?',
                                            'Do you really need to delete %s notebook?'% nbook.upper()
                                            )
            if answer:
                for notebook in os.listdir(NOTE_PATH):
                    filename, extension = os.path.splitext(notebook)
                    if filename == nbook:
                        notebook_file = os.path.join(NOTE_PATH, notebook)
                        if os.path.isfile(notebook_file):
                            os.remove(notebook_file)
            self.showNoteBook()
            self.sc_text.set_content("<html><head></head><body></body></html>")
            self.sc_text.style(STYLESHEET)
        else:
            print('Sorry notebook not found')

    def deleteNotes(self):
        note = self.note_tree.focus()
        result = tk.messagebox.askyesno('Delete Note?', 'Do you really want to delete this note?')
        if result == False:
            return
        
        if note != '':
            nbook = self.note_tree.parent(note)
            db = shelve.open(os.path.join(NOTE_PATH, nbook))
            flag = note in db
            if flag:
                del db[note]
            db.close()
        self.showNoteBook()
        self.sc_text.set_content(README)
        self.sc_text.style(STYLESHEET)
        
    def showNoteBook(self):
        """
        This function accept no argument and insert all the filenames of the
        the notebook into the treeview widget.
        """
        children = self.note_tree.get_children()
        if len(children) != 0:
            for child in children:
                self.note_tree.delete(child)
        list_of_files = os.listdir(NOTE_PATH)
        notebook = []
        for mfile in list_of_files:
            filename, extension = os.path.splitext(mfile)
            if filename not in notebook:
                notebook.append(filename)
                self.note_tree.insert('', 'end', filename, text=filename.title(), tags='treefont')
                add_notes = shelve.open(os.path.join(NOTE_PATH, filename))
                for key in add_notes:
                    self.note_tree.insert(filename, 'end', key, text=key, tags='treefont')
                    self.note_tree.set(key, 'date', add_notes[key]['date'])
                add_notes.close()

    def close(self):
        """ Destroy the widget and close the software. """
        self.master.destroy()

class NewNotebook(tk.Frame):

    def __init__(self, master=None, **kw):
        tk.Frame.__init__(self, master)
        self.master.title("New Notebook")
        self.master.protocol("WM_DELETE_WINDOW", self.close)
        self.master.geometry("+200+200")
        self.config(padx=50, pady=50)
        self.setupUi()
        if os.name == 'nt':
            self.master.iconbitmap(ICON)
        elif os.name == 'posix':
            self.master.tk.call('wm', 'iconphoto', self.master._w, tk.PhotoImage(file='isulatmo.gif'))

    def setupUi(self):
        self.notebook_entry = ttk.Entry(self)
        self.notebook_entry.pack(padx=10, pady=10)
        btn_frame = tk.Frame(self)
        btn_frame.pack(fill="x", padx=5, pady=5)
        self.save_button = ttk.Button(btn_frame, text="Save", command=self.saveNoteBook)
        self.save_button.grid(row=0, column=1)
        self.cancel_button = ttk.Button(btn_frame, text="Cancel", command=self.close)
        self.cancel_button.grid(row=0, column=0)
        self.notebook_entry.focus()

    def saveNoteBook(self):
        notebook = self.notebook_entry.get()
        if len(notebook) == 0:
            print('Sorry Something went wrong!')
        else:
            nb_file = shelve.open(os.path.join(NOTE_PATH, notebook))
            nb_file.close()
            self.master.focus_set()
            self.close()

    def close(self):
        self.master.destroy()

class NewNotes(tk.Frame):

    def __init__(self, master=None, notebook=None):
        tk.Frame.__init__(self, master)
        self.notebook = notebook
        self.master.title(self.notebook.title() + " - Add Notes")
        self.master.geometry("800x640+10+10")
        self.config(padx=5, pady=5)
        self.setupUi()
        if os.name == 'nt':
            self.master.iconbitmap(ICON)
            self.master.state('zoomed')
        elif os.name == 'posix':
            self.master.attributes('-zoomed', True)
            self.master.tk.call('wm', 'iconphoto', self.master._w, tk.PhotoImage(file='isulatmo.gif'))

    def setupUi(self):
        title_frame = tk.Frame(self)
        title_frame.pack(fill="x")
        title_lbl = tk.Label(title_frame, text="Title", fg="maroon",
                             font=('Georgia', 11, 'bold'))
        title_lbl.grid(row=0, column=0)
        self.title_entry = ttk.Entry(title_frame, font=('Georgia', 11))
        self.title_entry.grid(row=0, column=1)

        md_options = ['<h1>', '<h2>', '<h3>', '<h4>', '<h5>', '<h6>',
                      '<code>', '<bold>', '<italic>', '<blockquote>', '<hr>',
                      '<img>', '<a>']

        count = 0
        for col in range(2, 15):
            tk.Button(title_frame, text=md_options[count]).grid(row=0, column=col)
            count = count + 1

        self.bind_all("<Button-1>", self.toolBar)

        main_frame = tk.PanedWindow(self, orient=tk.HORIZONTAL)
        main_frame.pack(expand=True, fill=tk.BOTH)
        
        self.notes_text = ScrolledText(main_frame, wrap=tk.WORD, width=65)
        self.notes_text.bind('<KeyRelease>', self.updateDisplay)
        main_frame.add(self.notes_text)

        viewer_frame = tk.Frame(main_frame)

        self.html_viewer = HtmlViewer(viewer_frame)
        self.html_viewer.handlerImage(imageHandler)
        self.html_viewer.set_content("<html><head></head><body></body></html>")
        self.html_viewer.pack(expand=True, fill=tk.BOTH, side=tk.LEFT)

        self.v_scroll = tk.Scrollbar(viewer_frame, orient=tk.VERTICAL, command=self.html_viewer.yview)
        self.v_scroll.pack(fill='y', side=tk.RIGHT)

        self.html_viewer['yscrollcommand'] = self.v_scroll.set

        main_frame.add(viewer_frame)

        self.loadCSS()

        btn_frame = tk.Frame(self)
        btn_frame.pack(fill="x")
        self.save_button = ttk.Button(btn_frame, text="Save", command=self.saveNotes)
        self.save_button.grid(row=0, column=1)
        self.cancel_button = ttk.Button(btn_frame, text="Quit", command=self.close)
        self.cancel_button.grid(row=0, column=0)

        # Hack fix for scroll issue with windows operating system.
        if os.name == 'nt':
            self.html_viewer.bind_all('<MouseWheel>', self.scrollHack)

        self.title_entry.focus_set()

    def toolBar(self, event):
        if event.widget.winfo_class() == "Button":
            option = event.widget.cget('text')
            if option == "<h1>":
                self.notes_text.insert(tk.INSERT, "\n#")
            elif option == "<h2>":
                self.notes_text.insert(tk.INSERT, "\n##")
            elif option == "<h3>":
                self.notes_text.insert(tk.INSERT, "\n###")
            elif option == "<h4>":
                self.notes_text.insert(tk.INSERT, "\n####")
            elif option == "<h5>":
                self.notes_text.insert(tk.INSERT, "\n#####")
            elif option == "<h6>":
                self.notes_text.insert(tk.INSERT, "\n######")
            elif option == "<bold>":
                self.notes_text.insert("insert", "****")
                self.notes_text.mark_set(tk.INSERT, "insert -2c")
            elif option == "<italic>":
                self.notes_text.insert("insert", "**")
                self.notes_text.mark_set(tk.INSERT, "insert -1c")
            elif option == "<code>":
                self.notes_text.insert(tk.INSERT, "\n\t")
            elif option == "<blockquote>":
                self.notes_text.insert(tk.INSERT, "\n>")
            elif option == "<hr>":
                self.notes_text.insert(tk.INSERT, "\n***\n")
            elif option == "<img>":
                img_name = filedialog.askopenfilename(parent=self, title="Add Image",
                                                       initialdir=IMG_PATH)
                if img_name != "":
                    img_name = os.path.split(img_name)[1]
                    self.notes_text.insert(tk.INSERT, "\n\n![%s](%s)" % (img_name, img_name))
                else:
                    print(img_name)
            elif option == "<a>":
                try:
                    input_link = simpledialog.askstring("Insert Link",
                                                        "Please insert link",
                                                        parent=self)
                    if input_link != None:
                        self.notes_text.insert(tk.INSERT,
                                               "\n\n[%s](%s)" % (input_link, input_link))
                    else:
                        print(input_link)
                except tk.TclError as e:
                    print("input_link: %s" % e)
            try:
                self.notes_text.focus_set()
            except tk.TclError as e:
                print("self.notes_text: %s " % e)

    def scrollHack(self, event):
        """This method fix the scroll issue with windows operating system."""
        if isinstance(event.widget, str):
            testing = event.widget.split(".")[1:5]
            widget = self.html_viewer.nametowidget(event.widget.replace(".document", ""))
            if event.delta == -120:
                widget.yview_scroll(6, tk.UNITS)
            elif event.delta == 120:
                widget.yview_scroll(-6, tk.UNITS)

    def insertContent(self, content):
        self.notes_text.delete('1.0', tk.END)
        self.notes_text.insert('1.0', content)

    def insertHTML(self):
        data = self.notes_text.get('1.0', tk.END)
        if os.name == 'posix':
            content = markdown.markdown(data, extensions=["markdown.extensions.tables"])
        else:
            content = markdown.markdown(data, extensions=["tables"])
        self.html_viewer.set_content(content)
        return content

    def updateDisplay(self, event):
        self.insertHTML()
        self.loadCSS()
        self.html_viewer.update_idletasks()
        index = 1.0 - self.v_scroll.get()[1]
        self.html_viewer.yview_moveto(index)

    def loadCSS(self):
        self.html_viewer.style(STYLESHEET)

    def saveNotes(self):
        """
        This function test the the content and title if not empty saves into html
        document and skip otherwise.
        """
        # Get the contents of the scrolled text widget.
        content = self.notes_text.get(1.0, 'end')
        content = fixItSub(content, mode="html")
        title = self.title_entry.get()
        
        if len(content) == 0:
            return None
        elif len(title) == 0:
            return None
        else:
            # Convert the markdown content to HTML.
            if os.name == 'posix':
                content = markdown.markdown(content, extensions=["markdown.extensions.tables"])
            else:
                content = markdown.markdown(content, extensions=["tables"])
            # Open the self.notebook shelve file
            nb_file = shelve.open(os.path.join(NOTE_PATH, self.notebook))
            date = time.strftime(DATE_FORMAT, time.localtime())
            # Insert content and date of creation to shelve file.
            nb_file[title] = {'content': content,
                              'date': date}
            # Then close the file.
            nb_file.close()
            # Set the main window to focus.
            self.master.focus_set()
            # Then last after save has been done close the New Notes window.
            self.close()  

    def close(self):
        """ This closes the New Notes window. """
        self.unbind_all("<Button-1>")
        self.master.destroy()

class EditNotes(tk.Frame):

    def __init__(self, master=None, notebook=None, note_key=None):
        tk.Frame.__init__(self, master)
        self.notebook = notebook
        self.note_key = note_key
        self.master.title(self.notebook.title() + " - " + note_key)
        self.master.geometry("800x640+10+10")
        self.config(padx=5, pady=5)
        self.setupUi()
        if os.name == 'nt':
            self.master.state('zoomed')
            self.master.iconbitmap(ICON)
        elif os.name == 'posix':
            self.master.attributes('-zoomed', True)
            self.master.tk.call('wm', 'iconphoto', self.master._w, tk.PhotoImage(file='isulatmo.gif'))

    def setupUi(self):
        if self.notebook == None or self.note_key == None:
            self.close()
        else:
            db = shelve.open(os.path.join(NOTE_PATH, self.notebook))
            content = db[self.note_key]['content']
            title = self.note_key
            db.close()
            
            title_frame = tk.Frame(self)
            title_frame.pack(fill="x")
            title_lbl = tk.Label(title_frame, text="Title", fg="maroon",
                                 font=('Georgia', 11, 'bold'))
            title_lbl.grid(row=0, column=0)
            self.title_entry = ttk.Entry(title_frame, font=('Georgia', 11))
            self.title_entry.grid(row=0, column=1)
            self.title_entry.insert('end', title)

            md_options = ['<h1>', '<h2>', '<h3>', '<h4>', '<h5>', '<h6>',
                          '<code>', '<bold>', '<italic>', '<blockquote>', '<hr>',
                          '<img>', '<a>']

            count = 0
            for col in range(2, 15):
                tk.Button(title_frame, text=md_options[count]).grid(row=0, column=col)
                count = count + 1

            self.bind_all("<Button-1>", self.toolBar)

            main_frame = tk.PanedWindow(self, orient=tk.HORIZONTAL)
            main_frame.pack(expand=True, fill=tk.BOTH)
            
            self.notes_text = ScrolledText(main_frame, wrap=tk.WORD, width=65)
            self.notes_text.bind('<KeyRelease>', self.updateDisplay)
            main_frame.add(self.notes_text)
            
            myparser = HTML2Text()
            myparser.body_width = 0
            content = fixItSub(myparser.handle(content), mode="text")
            self.notes_text.insert('end', content)

            viewer_frame = tk.Frame(main_frame)

            self.html_viewer = HtmlViewer(viewer_frame)
            self.html_viewer.handlerImage(imageHandler)
            self.html_viewer.pack(expand=True, fill=tk.BOTH, side=tk.LEFT)

            self.v_scroll = tk.Scrollbar(viewer_frame, orient=tk.VERTICAL, command=self.html_viewer.yview)
            self.v_scroll.pack(fill='y', side=tk.RIGHT)

            self.html_viewer['yscrollcommand'] = self.v_scroll.set

            main_frame.add(viewer_frame)

            self.insertHTML()
            self.loadCSS()

            # Hack fix for scroll issue with windows operating system.
            if os.name == 'nt':
                self.html_viewer.bind_all('<MouseWheel>', self.scrollHack)

            btn_frame = tk.Frame(self)
            btn_frame.pack(fill="x")
            self.save_button = ttk.Button(btn_frame, text="Save", command=self.saveNotes)
            self.save_button.grid(row=0, column=1)
            self.cancel_button = ttk.Button(btn_frame, text="Cancel", command=self.close)
            self.cancel_button.grid(row=0, column=0)

            self.bind_all('<Control-KeyPress-S>', self.saveNotesEvent)
            self.bind_all('<Control-KeyPress-s>', self.saveNotesEvent)

            self.notes_text.focus_set()
            self.notes_text.see(tk.END)

    def toolBar(self, event):
        if event.widget.winfo_class() == "Button":
            option = event.widget.cget('text')
            if option == "<h1>":
                self.notes_text.insert(tk.INSERT, "\n#")
            elif option == "<h2>":
                self.notes_text.insert(tk.INSERT, "\n##")
            elif option == "<h3>":
                self.notes_text.insert(tk.INSERT, "\n###")
            elif option == "<h4>":
                self.notes_text.insert(tk.INSERT, "\n####")
            elif option == "<h5>":
                self.notes_text.insert(tk.INSERT, "\n#####")
            elif option == "<h6>":
                self.notes_text.insert(tk.INSERT, "\n######")
            elif option == "<bold>":
                self.notes_text.insert("insert", "****")
                self.notes_text.mark_set(tk.INSERT, "insert -2c")
            elif option == "<italic>":
                self.notes_text.insert("insert", "**")
                self.notes_text.mark_set(tk.INSERT, "insert -1c")
            elif option == "<code>":
                self.notes_text.insert(tk.INSERT, "\n\t")
            elif option == "<blockquote>":
                self.notes_text.insert(tk.INSERT, "\n>")
            elif option == "<hr>":
                self.notes_text.insert(tk.INSERT, "\n***\n")
            elif option == "<img>":
                img_name = filedialog.askopenfilename(parent=self, title="Add Image",
                                                       initialdir=IMG_PATH)
                if img_name != "":
                    img_name = os.path.split(img_name)[1]
                    self.notes_text.insert(tk.INSERT, "\n\n![%s](%s)" % (img_name, img_name))
                else:
                    print(img_name)
            elif option == "<a>":
                try:
                    input_link = simpledialog.askstring("Insert Link",
                                                        "Please insert link",
                                                        parent=self)
                    if input_link != None:
                        self.notes_text.insert(tk.INSERT,
                                               "\n\n[%s](%s)" % (input_link, input_link))
                    else:
                        print(input_link)
                except tk.TclError as e:
                    print("input_link: %s" % e)
            try:
                self.notes_text.focus_set()
            except tk.TclError as e:
                print("self.notes_text: %s " % e)

    def scrollHack(self, event):
        """This method fix the scroll issue with windows operating system."""
        if isinstance(event.widget, str):
            widget = self.html_viewer.nametowidget(event.widget.replace(".document", ""))
            if event.delta == -120:
                widget.yview_scroll(6, tk.UNITS)
            elif event.delta == 120:
                widget.yview_scroll(-6, tk.UNITS)

    def insertContent(self, content):
        self.notes_text.delete('1.0', tk.END)
        self.notes_text.insert('1.0', content)

    def insertHTML(self):
        data = self.notes_text.get('1.0', tk.END)
        if os.name == 'posix':
            content = markdown.markdown(data, extensions=["markdown.extensions.tables"])
        else:
            content = markdown.markdown(data, extensions=["tables"])
        self.html_viewer.set_content(content)

    def updateDisplay(self, event):
        self.insertHTML()
        self.loadCSS()
        self.html_viewer.update_idletasks()
        index = self.notes_text.vbar.get()[0]
        self.html_viewer.yview_moveto(index)

    def loadCSS(self):
        self.html_viewer.style(STYLESHEET)

    def saveNotes(self):
        """
        This function test the the content and title if not empty saves into html
        document and skip otherwise.
        """
        # Get the contents of the scrolled text widget.
        content = self.notes_text.get(1.0, 'end')
        content = fixItSub(content, mode="html")
        title = self.title_entry.get()
        
        if len(content) == 0:
            return None
        elif len(title) == 0:
            return None
        else:
            # Delete the title and content first
            db = shelve.open(os.path.join(NOTE_PATH, self.notebook))
            del db[self.note_key]
            db.close()
            
            # Convert the markdown content to HTML.
            if os.name == 'posix':
                content = markdown.markdown(content, extensions=["markdown.extensions.tables"])
            else:
                content = markdown.markdown(content, extensions=["tables"])
            # Open the self.notebook shelve file
            nb_file = shelve.open(os.path.join(NOTE_PATH, self.notebook))
            date = time.strftime(DATE_FORMAT, time.localtime())
            # Insert content and date of creation to shelve file.
            nb_file[title] = {'content': content,
                              'date': date}
            # Then close the file.
            nb_file.close()

            check = messagebox.askyesno('Continue', 'Do you want to continue editing?')

            if check == False:
                # Set the main window to focus.
                self.master.focus_set()
                # Then last after save has been done close the New Notes window.
                self.close()
            else:
                self.notes_text.focus_set()
                self.notes_text.see(tk.END)

    def saveNotesEvent(self, event):
        """
        This function test the the content and title if not empty saves into html
        document and skip otherwise. This was callable if the user press and hold
        Control + S or Control + s.

        """
        # Get the contents of the scrolled text widget.
        content = self.notes_text.get(1.0, 'end')
        content = fixItSub(content, mode="html")
        title = self.title_entry.get()
        
        if len(content) == 0:
            return None
        elif len(title) == 0:
            return None
        else:
            # Delete the title and content first
            db = shelve.open(os.path.join(NOTE_PATH, self.notebook))
            del db[self.note_key]
            db.close()
            
            # Convert the markdown content to HTML.
            if os.name == 'posix':
                content = markdown.markdown(content, extensions=["markdown.extensions.tables"])
            else:
                content = markdown.markdown(content, extensions=["tables"])
            # Open the self.notebook shelve file
            nb_file = shelve.open(os.path.join(NOTE_PATH, self.notebook))
            date = time.strftime(DATE_FORMAT, time.localtime())

            # Insert content and date of creation to shelve file.
            nb_file[title] = {'content': content,
                              'date': date}
            # Then close the file.
            nb_file.close()

            check = messagebox.askyesno('Continue', 'Do you want to continue editing?')

            if check == False:
                # Set the main window to focus.
                self.master.focus_set()
                # Then last after save has been done close the New Notes window.
                self.close()
            else:
                self.notes_text.focus_set()
                self.notes_text.see(tk.END)
                

    def close(self):
        """ This closes the Edit Notes window. """
        self.master.destroy()

class About(tk.Frame):

    def __init__(self, master=None, **kw):
        tk.Frame.__init__(self, master)
        self.master.title('About Notebook')
        self.master.geometry('320x280')
        self.setupUi(**kw)
        self.config(padx=10, pady=10)
        self.master.update_idletasks()
        scr_height = self.master.winfo_screenheight()
        scr_width = self.master.winfo_screenwidth()
        pheight = self.master.winfo_height()
        pwidth = self.master.winfo_width()
        height = int(scr_height/2) - int(pheight/2)
        width = int(scr_width/2) - int(pwidth/2)
        self.master.geometry('+'+str(width)+'+'+str(height))
        if os.name == 'nt':
            self.master.iconbitmap(ICON)
        elif os.name == 'posix':
            self.master.tk.call('wm', 'iconphoto', self.master._w, tk.PhotoImage(file='isulatmo.gif'))

    def setupUi(self, **kws):
        self.logo_img = Image.open(kws['logo'])
        self.logo_img_open = ImageTk.PhotoImage(self.logo_img.resize((48, 48)))
        tk.Label(self, image=self.logo_img_open).pack()
        tk.Label(self, text=kws['name'], fg='maroon', font=('Georgia', 15, 'bold')).pack()
        tk.Label(self, text='version: ' + kws['version'], font=('Georgia', 10)).pack()
        tk.Label(self, text=kws['description'], font=('Georgia', 10)).pack()
        tk.Label(self, text="", font=('Georgia', 10)).pack()
        tk.Label(self, text='Author: ' + kws['author'], font=('Georgia', 9)).pack()
        tk.Label(self, text='E-mail: ' + kws['email'], font=('Georgia', 9)).pack()
        tk.Label(self, text='Web: ' + kws['web'], font=('Georgia', 9)).pack()
        btn_frame = tk.Frame(self)
        btn_frame.pack(padx=5, pady=5)
        close_btn = ttk.Button(btn_frame, text="Close", command=self.master.destroy)
        close_btn.pack(side=tk.RIGHT)
        lic_btn = ttk.Button(btn_frame, text="License")
        lic_btn.pack(side=tk.LEFT)
        lic_btn.config(command=self.showLicense)
        close_btn.focus()

    def showLicense(self):
        lic_tp = tk.Toplevel(self)
        config = {'license': License_Path}
        win = LicenseWindow(lic_tp, **config)
        win.pack(expand=True, fill=tk.BOTH)


class HelpWindow(tk.Frame):

    def __init__(self, master=None, **kw):
        tk.Frame.__init__(self, master)
        self.master.geometry('+150+20')
        self.master.title('Help')
        self.setupUi(kw['help_file'])
        self.config(padx=10, pady=10)
        if os.name == 'nt':
            self.master.iconbitmap(ICON)
        elif os.name == 'posix':
            self.master.tk.call('wm', 'iconphoto', self.master._w, tk.PhotoImage(file='isulatmo.gif'))

    def setupUi(self, helpfile):
        with open(helpfile, 'r') as mdfile:
            data = mdfile.read()

        if os.name == 'posix':
            html_convert = markdown.markdown(data, extensions=['markdown.extensions.tables'])
        else:
            html_convert = markdown.markdown(data, extensions=["tables"])

        container = tk.Frame(self)
        container.pack(expand=True, fill=tk.BOTH)

        self.helpframe = HtmlViewer(container)
        self.helpframe.pack(expand=True, fill=tk.BOTH, side=tk.LEFT)
        self.helpframe.handlerImage(imageHandler)
        self.helpframe.set_content(html_convert)
        self.helpframe.style(STYLESHEET)

        vsb = tk.Scrollbar(container, orient=tk.VERTICAL, command=self.helpframe.yview)
        vsb.pack(fill='y', side=tk.RIGHT)

        self.helpframe['yscrollcommand'] = vsb.set

        close_btn = ttk.Button(self, text="Close", command=self.closeHelp)
        close_btn.pack(side=tk.BOTTOM)
        close_btn.focus()

        # Hack fix for windows operating system.
        if os.name == 'nt':
            self.helpframe.bind_all("<MouseWheel>", self.scrollHack)

    def scrollHack(self, event):
        """This method fix the scroll issue with windows operating system."""
        if isinstance(event.widget, str):
            widget = self.helpframe.nametowidget(event.widget.replace(".document", ""))
            if event.delta == -120:
                widget.yview_scroll(6, tk.UNITS)
            elif event.delta == 120:
                widget.yview_scroll(-6, tk.UNITS)

    def closeHelp(self):
        self.master.destroy()
        

class LicenseWindow(tk.Frame):

    def __init__(self, master=None, **kw):
        tk.Frame.__init__(self, master)
        self.master.geometry('+100+200')
        self.master.title('License')
        self.setupUi(kw['license'])
        self.config(padx=10, pady=10)
        if os.name == 'nt':
            self.master.iconbitmap(ICON)
        elif os.name == 'posix':
            self.master.tk.call('wm', 'iconphoto', self.master._w, tk.PhotoImage(file='isulatmo.gif'))

    def setupUi(self, license_file):
        lic_text = ScrolledText(self, foreground='#1F5889', background='#C7E5FF')
        lic_text.pack(expand=True, fill=tk.BOTH)

        with open(license_file, 'r') as licfile:
            data = licfile.read()

        lic_text.insert('end', data)
        lic_text.config(state="disabled")
        
        close_btn = ttk.Button(self, text="Close", command=self.master.destroy)
        close_btn.pack()
        close_btn.focus()

def main():
    """ This is the main function of the program. """
    # Check first if the appropriate path of the notes is available. If not
    # create a folder 'notes' for files storage.
    if not os.path.isdir(os.path.join(os.path.expanduser('~'), 'Documents', 'IsulatMo')):
        os.mkdir(os.path.join(os.path.expanduser('~'), 'Documents', 'IsulatMo'))
    if not os.path.isdir(NOTE_PATH):
        os.mkdir(NOTE_PATH)
    if not os.path.isdir(IMG_PATH):
        os.mkdir(IMG_PATH)
        src = os.path.join(os.getcwd(), 'isulatmo.gif')
        dst = os.path.join(IMG_PATH, 'isulatmo.gif')
        try:
            shutil.copy2(src, dst)
        except:
            print(sys.exc_info())
    # Load the graphical user interface.
    app = tk.Tk()
    window = Notebook(app)
    window.pack(expand=True, fill=tk.BOTH)
    app.mainloop()

if __name__ == "__main__":
    sys.exit(main())
