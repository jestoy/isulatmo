# IsulatMo

	version: 1.0.0

---

## Welcome

Isulatmo is a simple virtual notebook for storing your thoughts and other
concerns. It may also serve as a diary of events or knowledge you need
to remember. This program was created using the Python Programming Language.
It uses markdown format to add notes to your notebooks and later will be
converted to html format when save to be able to render it in designated
html viewer. Image may also be included but bear in mind that you have to
resize the image to it's desired size so that it will not be rendered too big
or too small in the viewer. Creating link is not yet implemented.

## Installation:
Installation is very easy just extract the content of **isulatmo.zip** to
anywhere in your directory. Look for the **isulatmo.exe** in windows and
**isulatmo** in Linux and double click on it and you are ready to go.

## Markdown Cheat Sheet

### Headers

	# H1
	## H2
	### H3
	#### H4
	##### H5
	###### H6

# H1
## H2
### H3
#### H4
##### H5
###### H6

### Emphasis

	Emphasis, aka italics, with *asterisks* or _underscores_.
	
	Strong emphasis, aka bold, with **asterisks** or __underscores__.
	
	Combined emphasis with **asterisks and _underscores_**.
	
Emphasis, aka italics, with *asterisks* or _underscores_.

Strong emphasis, aka bold, with **asterisks** or __underscores__.

Combined emphasis with **asterisks and _underscores_**.

### Lists

	Ordered List

	1. First order list
	2. Second order list
	3. Third order list
	
	Unordered List

	* First unordered list
	* Second unordered list
	* Third unordered list

**Ordered List**

1. First order list
2. Second order list
3. Third order list

**Unordered List**

* First unordered list
* Second unordered list
* Third unordered list

### Images

	![Alt Text](isulatmo.gif)

![Alt Text](isulatmo.gif)

### Quote

	> This is a sample quote.

> This is a sample quote.

### Html tag

	<h2>Hello, World in html tag.</h2>
	
	<img src="isulatmo.gif" width="100" height="100" alt="Alt Text" />
	
	<table>
		<tr>
			<th>First Column</th>
			<th>Second Column</th>
		</tr>
        <tr>
            <td>Content1</td>
            <td>Content2</td>
        </tr>
	</table>
    
    <p>A super script example: x<sup>2+1</sup></p>
    <p>A subscript example: y<sub>2+1</sub></p>

<h2>Hello, World in html tag.</h2>

<img src="isulatmo.gif" width="100" height="100" alt="Alt Text" />

<table>
	<tr>
		<th>First Column</th>
		<th>Second Column</th>
	</tr>
    <tr>
        <td>Content1</td>
        <td>Content2</td>
    </tr>
</table>

<p>A superscript example: x<sup>2+1</sup></p>
<p>A subscript example: y<sub>2+1</sub></p>

## Table using markdown.

    First Column | Second Column  
    ---|---  
    Content1 | Content2

First Column | Second Column  
---|---  
Content1 | Content2

### Creating Link

    Link is not yet implated, hence you may not been able to use it except for display purpose.  
    [Google](https://www.google.com/)

[Google](https://www.google.com/)

### Horizontal Rule

	Three or more of the following.
	
	---
	
	Hypens
	
	***
	
	Asterisks
	
	___
	
	Underscores
	
Three or more of the following.
	
---

Hypens

***

Asterisks

___

Underscores

## Credit

[Nguyen Van Hieu - Hieunv1996](mailto://hieunv.dev@gmail.com)

## To do
